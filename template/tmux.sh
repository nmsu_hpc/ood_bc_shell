#!/usr/bin/env bash

# Load the required modules
module load spack/2022a gcc/12.1.0-2022a-gcc_8.5.0-ivitefn
module load tmux/3.2a-2022a-gcc_12.1.0-jwjkrr7

# Set TMUX Environment Variables
TMUX_PATH="$(which --skip-alias --skip-functions tmux)"
TMUX_SOCKET="J${SLURM_JOB_ID}.sock"
TMUX_SESSION="J${SLURM_JOB_ID}"
export TMUX_PATH TMUX_SOCKET TMUX_SESSION

# Check if ${HOME}/.config/tmux/tmux.conf exists
if [ -f "${HOME}/.config/tmux/tmux.conf" ]; then
	TMUX_CONFIG_PATH="${HOME}/.config/tmux/tmux.conf"
# Check if ${HOME}/.tmux.conf exists
elif [ -f "${HOME}/.tmux.conf" ]; then
	TMUX_CONFIG_PATH="${HOME}/.tmux.conf"
else
	# Ensure the tmux config directory exists
	mkdir -p "${HOME}/.config/tmux"

	# If neither exists, copy the template config to ${HOME}/.config/tmux/tmux.conf
	cp -f "$OOD_STAGED_ROOT/tmux.conf" "${HOME}/.config/tmux/tmux.conf"
	TMUX_CONFIG_PATH="${HOME}/.config/tmux/tmux.conf"
	
	# Create the symlink in ${HOME}/.tmux.conf (pointing to ${HOME}/.config/tmux/tmux.conf)
	ln -sf "${HOME}/.config/tmux/tmux.conf" "${HOME}/.tmux.conf"
fi
export TMUX_CONFIG_PATH

# Set TMUX Function with full path to tmux
function tmux {
	"$TMUX_PATH" "$@"
}
export -f tmux

# Cleanup Module System
module reset
