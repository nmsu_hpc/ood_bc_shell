#!/usr/bin/env bash

# Benchmark info
echo "TIMING - Starting main script at: $(date)"

# Reset Module Environment To Default
module reset

# set the TERM
export TERM="xterm-256color"

# Source TTYD Setup
source "$OOD_STAGED_ROOT/ttyd.sh"

# CD To Home Directory
cd "$HOME" || exit 7

# Benchmark info
echo "TIMING - Starting ttyd at: $(date)"

# Turn on Command Tracing
set -x

# Launch ttyd
if [ "$OOD_T_MUX" == 'tmux' ]; then
	# Source TMUX Setup
	source "$OOD_STAGED_ROOT/tmux.sh"
	
	ttyd \
		-p "${PORT}" \
		-b "/node/${HOST}/${PORT}" \
		-t "fontSize=${OOD_FONT_SIZE}" \
		-t 'cursorStyle=bar' \
		-t 'cursorBlink=true' \
		-t 'theme={"background": "#282a36", "foreground": "#f8f8f2"}' \
		-c "ttyd:${PASSWORD}" \
		tmux -f "$TMUX_CONFIG_PATH" -L "$TMUX_SOCKET" new -A -s "$TMUX_SESSION"

	# kill tmux session server should ttyd crash
	tmux kill-server || true

elif [ "$OOD_T_MUX" == 'screen' ]; then
	# Source Screen Setup
	source "$OOD_STAGED_ROOT/screen.sh"

	ttyd \
		-p "${PORT}" \
		-b "/node/${HOST}/${PORT}" \
		-t "fontSize=${OOD_FONT_SIZE}" \
		-t 'cursorStyle=bar' \
		-t 'cursorBlink=true' \
		-t 'theme={"background": "#282a36", "foreground": "#f8f8f2"}' \
		-c "ttyd:${PASSWORD}" \
		"$OOD_STAGED_ROOT/screen_launcher.sh"
fi
