#!/usr/bin/env bash

# Set Screen Environment Variables
SCREEN_SESSION="J${SLURM_JOB_ID}"
export SCREEN_SESSION

# Check if ${HOME}/.screenrc exists
if [ ! -f "${HOME}/.screenrc" ]; then
    # If not, copy from $OOD_STAGED_ROOT/.screenrc
    cp -f "$OOD_STAGED_ROOT/.screenrc" "${HOME}/.screenrc"
fi

# Give the lander permission to run
chmod +x "$OOD_STAGED_ROOT/screen_launcher.sh"