#!/usr/bin/env bash

# Load the required modules
module load spack/2022a gcc/12.1.0-2022a-gcc_8.5.0-ivitefn
module load ttyd/1.7.0-2022a-gcc_12.1.0-3i53q3u

# Set TMUX Environment Variables
TTYD_PATH="$(which --skip-alias --skip-functions ttyd)"
export TTYD_PATH

# Set TMUX Function with full path to tmux
function ttyd {
	"$TTYD_PATH" "$@"
}
export -f ttyd

# Cleanup Module System
module reset
