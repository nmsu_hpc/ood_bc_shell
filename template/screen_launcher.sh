#!/usr/bin/env bash

# If screen with name $SCREEN_SESSION exists, reconnect to it
if screen -ls | grep -q "\.$SCREEN_SESSION\b"; then
    screen -d -r "$SCREEN_SESSION" 
else  # create the screen with name $SCREEN_SESSION
    screen -S "$SCREEN_SESSION" -c "${HOME}/.screenrc"
fi